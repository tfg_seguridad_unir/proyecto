<html>
<head>
	<style>
			p
				{font-size:1.1em; padding:4px;}	
			li
				{text-align:left;}		
	</style>
</head>
<body style="background-color:#D4D4D4; margin:40;padding:0">
<center><div style="border:5px solid; border-color:#B20000; width:800px" >
	<center><img src="imagenes/xss3.png" style="width:650px; height:200px; "></center>

<br>
<u><h1 align="center">Cross Site Scripting (XSS)</h1></u>

<p>El ataque XSS es, hoy d&iacutea, el tercer tipo de ataque m&aacutes frecuente en el conjunto de servicios web de Internet seg&uacuten el proyecto Top Ten de OWASP. Al igual que la mayor parte de ataques, XSS no tiene una &uacutenica forma de implementarse, sino que se define por la vulnerabilidad que explota.</p>

<p>Una diferencia fundamental de este ataque frente a los dem&aacutes que hemos visto hasta ahora es que XSS no emplea la vulnerabilidad en el servicio web para atacar el propio servidor, sino que sus v&iacutectimas son el resto de clientes (usuarios) del servicio. Esto significa que el alcance de un ataque de este tipo es directamente proporcional a la cantidad de gente que emplee el servicio y esto, a su vez, hace que sea un problema especialmente grave para los administradores de los servidores web que reciben mucho tr&aacutefico, al mismo tiempo que resulta muy rentable para los atacantes. En conjunto, se puede decir que XSS es muy prevalente, f&aacutecil de encontrar, independiente del lenguage que se haya empleado para desarrollar el servicio web y relativamente sencillo de implementar una vez que se encuentra un servidor vulnerable; El atacante s&oacutelo necesita unos conocimientos b&aacutesicos de Javascript. </p>

<hr>
<h2>Un ejemplo:</h2>
<p>Existen varias formas o subtipos de implementar este ataque, pero todos buscan distribuir scripts maliciosos entre los usuarios de una p&aacutegina web vulnerable. Aqu&iacute vamos a ver en detalle lo que se conoce como <i>stored XSS</i> o XSS <i>"almacenado"</i>. Los foros o tablones de mensajes son el blanco ideal para este ataque ya que permiten que cualquier participante del foro escriba contenido que ser&aacute mostrado en los navegadores de los dem&aacutes usuarios. El objetivo: Introducir c&oacutedigo Javascript (<a href="http://192.168.200.111/XSS_JS.php">Ver algunos ejemplos de Javascript</a>) en un mensaje que todo el mundo pueda ver. Esto har&aacute que todo el mundo que vea nuestro mensaje ejecute nuestro c&oacutedigo Javascript en sus navegadores.</p>

<video src="04-XSS.ogv"  controls autoplay" widht=600 height=900 ></video>
<p><u>Los pasos a seguir son:</u></p>
<ul>
	<li>Acceder al servicio web para poder colgar un mensaje.</li>
	<li>Escribir un mensaje que incluya c&oacutedigo Javascript.</li>
	<li>Acceder al tabl&oacuten de mensajes para comprobar que se ha implementado con &eacutexito.</li>
</ul>

<center><img src="imagenes/esquemaXSS.png" style="width:700px; height:600px; border:2px solid black"></center>
<hr>

<h2 style="font-size:1.7em; background-color:#66FF99;">Prueba t&uacute:</h2>

<p>A continuaci&oacuten se muestra un formulario que enlaza con el servicio web vulnerable. Este formulario nos permite hacer log-in con un usuario cualquiera que est&eacute registrado, para luego colgar un mensaje en el tabl&oacuten de mensajes. 
<form align="center" action="http://192.168.200.111/bienvenida.php" method=get>
		<center><b>Usuario: </b> <input type="text" name="usuario"></center><br>
		<center><b>Password: </b> <input type="text" name="pass"></center><br>
		<center><input type="submit" style="width:225px;height:35" name="enviarXSS" value="Acceder al servicio vulnerable"></center>
	</form>
<br>




<center><div style="background-color:#FFC8C8; border: 1px solid black; width:650px" align="center">
<p>Esta p&aacutegina web est&aacute preparada para servir como parte de una plataforma did&aacutectica. 
Forma parte del trabajo de fin de grado y en ning&uacuten caso pretende ser una herramienta para uso malicioso. </p></center>
</div><br><br></div></div></center>
<body>
</html>





