<?php
	if( isset( $_GET['lanzaTroy'] ) )
	{
		$fichero = "/root/troyano/lanzar";
		file_put_contents($fichero, "1");
		header("Location: http://192.168.200.111/ataques.php");
	} else 
	{  
		$fichero = "/root/troyano/lanzar";
		file_put_contents($fichero, "0");
	}
	
?>

<html>
<head>
	<style>
			p
				{font-size:1.1em; padding:4px;}	
			li
				{text-align:left;}		
	</style>
</head>
<body style="background-color:#D4D4D4; margin:40;padding:0">
<center><div style="border:5px solid; border-color:#B20000; width:800px" >
	<center><img src="imagenes/troy.jpg" style="width:350px; height:200px; "></center>


<u><h1 align="center">Explotar un Programa Troyano</h1></u>
<p>La mayor parte de los hosts conectados a internet se encuentran protegidos por dispositivos o programas de serguridad perimetral como routers y firewalls. Hoy d&iacutea, incluso los sistemas operativos de escritorio tienen firewall configurados por defecto. Una de las reglas m&aacutes b&aacutesicas de estos programas de seguridad es bloquear toda la comunicaci&oacuten entrante que no forme parte de una conversaci&oacuten iniciada desde dentro.</p>
<p>Los programas conocidos como troyanos aparentan ser un programa leg&iacutetimo para conseguir que el usuario los instale pero, adem&aacutes de su funcionalidad aparente, ejecutan c&oacutedigo malicioso que inicia comunicaciones con el atacante de manera que la protecci&oacuten del cortafuegos no las detiene.</p>
<center><img src="imagenes/troyDiag.png" style="width:700px; height:200px; border:2px solid black"></center>
<hr>
<h2>Un ejemplo:</h2>
<p>Vamos a ver un programa troyano en acci&oacuten. En esta implementaci&oacuten, la explotaci&oacuten del troyano requiere dos scripts. Uno de ellos, <a href="troyano/callhome">el script malicioso</a>, se est&aacute ejecutando en la m&aacutequina vulnerable. El <a href="troyano/revshell">otro script</a> lo tenemos nosotros como atacantes. Nuestro script nos servir&aacute como una interfaz rudimentaria para comunicarnos con el troyano que ha infectado el servidor vulnerable, y decirle qu&eacute comandos queremos que ejecute.</p>
<p>Recuerda que la idea principal es conseguir acceso a la shell del sistema vulnerable <u>a trav&eacutes de una conexi&oacuten iniciada desde &eacutel.</u>Estos dos scripts, trabajando juntos, siguen la siguiente din&aacutemica (ver diagrama):</p>
<ul>
	<li>El programa atacante, vigila el fichero salida.</li>
	<li>El programa del lado de la v&iacutectima vigila el fichero comando.</li>
	<li>Cuando el atacante escribe un comando en el fichero comando (en el PC atacante), el troyano lo lee, y lo ejecuta en el sistema de la v&iacutectima. La salida del comando ejecutado no se muestra por pantalla, sino que se escribe en el fichero salida (tambi&eacuten en el PC atacante).</li>
	<li>El programa atacante se percata del contenido nuevo en el fichero salida y lo muestra por pantalla en el PC del atacante.</li>
	<li>Todas las conexiones son iniciadas por el sistema v&iacutectima.</li>
	<li>El programa del lado del atacante &uacutenicamente lee y escribe sobre ficheros locales.</li>
</ul>

<center><img src="imagenes/troyDiag2.png" style="width:700px; height:450px; border:2px solid black"></center>

<br><br>
<h2 style="font-size:1.7em; background-color:#66FF99;">Prueba t&uacute:</h2>
<p>Para manejar el programa troyano que se est&aacute ejecutando en el servidor, hay que emplear un terminal. Lo que el script local proporciona es un prompt en ese terminal desde el cual podemos ejecutar casi cualquier comando en el sistema remoto.</p>
<video src="02-Trojan.ogv"  controls autoplay" widht=600 height=900 ></video> 
<p>Te toca:</p
<center><form action="consolaTroyano.php" method="get">
	<input style="width:260px; height:60px" type="submit" name="lanzaTroy" value="Lanzar RevShell!">
</form></center>



</body>
</html>