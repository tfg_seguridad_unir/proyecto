<html>
<head>
	<style>
			p
				{font-size:1.1em; padding:4px;}			
	</style>
</head>
<body style="background-color:#D4D4D4; margin:40;padding:0">
<center><div style="border:5px solid; border-color:#B20000; width:900px" >
	<center><img src="imagenes/inyec.jpg" style="width:500px; height:200px; "></center>


<u><h1 align="center">Inyecci&oacuten SQL</h1></u>
<p>Este tipo de ataque consiste en emplear expresiones SQL para rellenar campos que asumimos que formar&aacuten parte de una sentencia que el motor de php lanzar&aacute contra la base de datos. La manera m&aacutes frecuente de implementar el ataque es a trav&eacutes de los formularios que las p&aacuteginas ofrecen.</p>
<hr>
<h2>Un ejemplo:</h2>
<p>Sabemos que hay un usuario que se llama psanchez, pero no sabemos su contrase&ntilde;a. Lo que s&iacute sabemos es que la p&aacutegina recoge la informaci&oacuten que introducimos en el formulario como una variables y la emplea para completar una sentencia SQL. Lo normal es que no sepamos c&oacutemo es esa sentencia, pero sabemos que tiene que ser algo parecido a: <strong>select id_usuario from usuarios where usuario like '$usuario' and pass = '$pass' </strong>. Introduciendo c&oacutedigo sql en cualquiera de los dos campos, podemos alterar el funcionamiento normal de la consulta. En este caso, introducimos un poquito de c&oacutedigo SQL (<strong>xxx' or '1'='1</strong>) en el campo de la contrase&ntilde;a para que la consulta SQL que se lanza contra el servidor SQL pase de ser as&iacute:</p>

<p align="center"><strong>select id_usuario from usuarios where usuario like '$usuario' and pass = '$pass'</strong></p>

<p> a ser as&iacute:</p>

<p align="center"><strong>select id_usuario from usuarios where usuario like '$usuario' and pass = '<span style="font-size:12pt; color:red; background-color:yellow">xxx' or '1'='1</span>' </strong></p>
<p>Como se puede ver, ahora hay una condici&oacuten que siempre se cumple de manera que no importa si la pass no es "xxx", porque la parte tras el "OR" devuelve TRUE. Esto puesto en la URL para el usuario "psanchez" queda as&iacute:</p>

<p align="center" style="font-size:0.9em"><strong>http://192.168.200.111/bienvenida.php?usuario=psanchez&pass=xxx%27+or+%271%27%3D%271&enviar=Acceder</strong></p>

<video src="01-SQL-Injection.ogv"  controls autoplay" widht=600 height=900 ></video>

<h2 style="font-size:1.7em; background-color:#66FF99;">Prueba t&uacute:</h2>
<p>A continuaci&oacuten se muestra el formulario de entrada del servicio web vulnerable. Escoge cualquier nombre de usuario que sepas que existe en la base de datos, introd&uacutecelo aqu&iacute y pulsa enviar. Luego <b>Observa la URL que se genera</b> y c&oacutemo da acceso.</p>
	<form align="center" action="http://192.168.200.111/bienvenida.php" method=get>
		<center><b>Usuario: </b> <input type="text" name="usuario"></center><br>
		<center><b>Password: </b> <input type="text" name="pass" value="xxx' or '1'='1" ></center><br>
		<center><input type="submit" style="width:95px;height:35" name="enviarI" value="Acceder"></center>
	</form>

<center><div style="background-color:#FFC8C8; border: 1px solid black; width:650px" align="center">
<p>Esta p&aacutegina web est&aacute preparada para servir como parte de una plataforma did&aacutectica. 
Forma parte del trabajo de fin de grado y en ning&uacuten caso pretende ser una herramienta para uso malicioso. </p></center>
</div><br><br></div></div></center>
<body>
</html>

