<?php
	if( isset( $_GET['lanzaBrute'] ) )
	{
		$fichero = "/root/troyano/lanzar";
		file_put_contents($fichero, "2");
		header("Location: http://192.168.200.111/ataques.php");
	} else 
	{  
		$fichero = "/root/troyano/lanzar";
		file_put_contents($fichero, "0");
	}
?>

<html>
<head>
	<style>
			p
				{font-size:1.1em; padding:4px;}	
			li
				{text-align:left;}		
	</style>
</head>
<body style="background-color:#D4D4D4; margin:40;padding:0">
<center><div style="border:5px solid; border-color:#B20000; width:800px" >
	<center><img src="imagenes/brute.jpg" style="width:800px; height:200px; "></center>
<u><h1 align="center">Ataque de Fuerza Bruta</h1></u>
<p>El ataque de fuerza bruta consiste en averiguar la contrase&ntilde;a para un usuario a través de prueba y error. Los ataques de fuerza bruta se sirven de la automatizaci&oacuten de este proceso para probar todas las combinaciones de caracteres posibles hasta dar con la correcta.</p>
<p>Dada una longitud de contrase&ntilde;a, el atacante probar&aacute, de media, la mitad de las contrase&ntilde;as posibles antes de dar con la correcta. Pero si la clave es especialmente corta, el atacante podr&iacutea tardar mucho menos. Por el contrario, si la contrase&ntilde;a es larga y combina distintos tipos de caracteres, el atacante podr&iacutea invertir todo su poder computacional durante a&ntilde;os sin dar con ella.</p>
<br><hr><br><h2>Un ejemplo:</h2>
<p>Un formulario web de acceso como el que nos ofrece la p&aacutegina web del servidor vulnerable puede ser atacada por fuerza bruta.</p>
<p>Cuando terminamos de rellenar el formulario y pinchamos en el bot&oacuten de enviar, se lanza una petici&oacuten GET al servidor. Esta petici&oacuten se hace en la forma de una URL que contiene los valores introducidos y que podemos ver en la parte superior cuando enviamos el formulario. Nosotros vamos a emplear un m&eacutetodo automatizado para lanzar muchas peticiones GET con el mismo nombre de usuario, pero distinta contrase&ntilde;a.</p>
<p>Para hacer esto, usaremos <a href="fuerzab/fuerzab"><b>un script</b></a> que emplea una funci&oacuten recursiva junto con el comando "wget" para enviar las peticiones al servidor. El script examina la p&aacutegina que le devuelve y, si no tiene la palabra "bienvenido", no se detiene.</p>
<br>
<video src="03-Brute_Force.ogv"  controls autoplay" widht=600 height=900 ></video>
<br>
<br>
<center><form action="consolaFBruta.php" method="get">
	<input style="width:260px; height:60px" type="submit" name="lanzaBrute" value="Lanzar Fuerza Bruta!">
</form></center>

</body>
</html>