<?php
session_start();
?>

<html>
<head>

	<style>
			#titulo
				{font-size:2.5em; font-family:serif; font-style: italic; text-shadow: 2px 2px #FF3333; position:relative;
				bottom:8px;}
			#nombre
				{font-size:0.7em; font-family:serif; font-style: italic; text-shadow: 2px 2px #FF3333; position:relative;
				bottom:8px;}
			#TFG
				{position:relative; bottom:8px; left:80px}
	</style>
</head>
<body style="background-color:#E3FFE3;">
<br>
	<center><div  style="background-image: url('./imagenes/unir.png'); border:1px solid black; width:800px; height:100px; ">
		<h1 id="TFG" style="color:white;"><span id="titulo" >TFG</span>
		<span id="nombre">Pablo Castellanos</span></h1>	
	</div></center><br>

	


<?php

//Primero compruebo que vengo del formulario.
if( !isset( $_GET['enviar'] ) && !isset( $_GET['enviarI']) && !isset( $_GET['enviarXSS']) && !isset( $_POST['verMensajes']))
{
	echo "<br>No se ha enviado el formulario.";
	die();	
}

if( isset( $_POST['verMensajes'] ))
{
	$enlace = "<script>window.open('http://192.168.200.111/tablon.php')</script>";
	echo $enlace;
}

if( isset( $_POST['enviarXSS'] ))
{
	$_SESSION['cuadro'] = 1;
}

if( isset( $_GET['enviar'] ) || isset( $_GET['enviarI']) )
{
	$_SESSION['cuadro'] = 0;
}	


//Abro la pestaña con la lista de anomalías detectadas.
if (isset( $_GET['enviarI'] ))
{
	echo "<script type='text/javascript'>";
	echo "window.open('http://192.168.200.111/ataques.php');";
	echo "</script>";
}

	$host = "localhost";
	$bd_usuario = "root";
	$bd_pass = "admin123";
	$bd = "paginaweb";
	$tabla = "usuarios";
	
	//Comprobar que los campos no están en blanco
	if( $_GET['usuario'] == "" || $_GET['pass'] == "" )
	{
		echo "<br><center><div style='background-color: #FF6F6F; width:800px; border:1px solid black;'><span style='font-size:2em'>No se han introducido todos los campos. <br><a href='entrada.php'>VOLVER</a></span></div></center>";
		die();	
	}

	//Guardo el username y la pass en variables
	$usuario = trim($_GET['usuario']);
	$pass = $_GET['pass'];

		
	//COmprobación de la existencia de comillas que indican inyección.
	if ( strpos($usuario, "'") !== false || strpos($pass, "'") !== false)
	{
		# Hay comilla simple en la entrada.
		$ficheroLog = '/var/log/hacks/hacks.log';
		$fecha = date("Y-m-d H:i:s");
		$mensaje = "$fecha"." ==>  <b>POSIBLE INYECCI&Oacute;N SQL</b>: Se ha detectado caracter especial  en el <b>formulario de entrada</b> -- Usuario: <b><span style='color:red'>$usuario</span></b>, Pass: <b><span style='color:red'>$pass</span></b>\n";
		file_put_contents($ficheroLog, $mensaje, FILE_APPEND);
	}
	
	
	//Tambien guardo el nombre de usuario como variable de sesión.
	$_SESSION['usuario'] = $usuario; 
	$_SESSION['pass'] = $pass; 
	//echo "debug1<br>";
	//Conexion con el servidor.
	$enlace = mysqli_connect($host, $bd_usuario, $bd_pass, $bd) or die("Error de conexion");
	

//---------------------------
	//Ejecutar la query para buscar filas con los datos de usuario y contrasenya introducidos.
	$query = "select id_usuario from usuarios where usuario like '$usuario' and pass = '$pass'";
	if( !($salida = mysqli_query($enlace, $query)) )
	{
		die( "Error al consultar la tabla $bd.$tabla: <br><br>".mysql_error() );
	}
	
	//Si existe el usuario en la BD, significa que hay una coicidencia   
	if ( mysqli_num_rows($salida) > 0 )
	{
		echo "<center><span style='font-size:3em; color:#2EB82E'><b>Acceso Concedido</b></span><span style='font-size:1.7em; color:#FF6600'> -- <a href='entrada.php'><b>Salir</b></a></span></center>";				
	}
	//Si no hay filas con esas condiciones no permitimos autenticacion.
	else
	{
		echo "<center><div style='background-color: #FF6F6F; width:800px; border:1px solid black;'><span style='font-size:2em'>Credenciales incorrectas<br><a href='entrada.php'>VOLVER</a></span></div></center>";

		//Llevo la cuenta de las solicitudes fallidas
		$ficheroIntentos = '/var/log/hacks/intentos';
		$intentos = file_get_contents($ficheroIntentos);
		echo "$intentos";
		if( $intentos > 800 )
		{	
			$ficheroLog = '/var/log/hacks/hacks.log';
			$fecha = date("Y-m-d H:i:s");
			$mensaje = "$fecha"." ==>  <b>ATAQUE DE FUERZA BRUTA EN CURSO</b>: Se han detectado demasiados intentos fallidos de acceso.</b>\n";
			file_put_contents($ficheroLog, $mensaje, FILE_APPEND);
			file_put_contents($ficheroIntentos, "0");
		}
		else
		{				
			$intentos = $intentos + 1;
			file_put_contents($ficheroIntentos, $intentos);
		}

		die();
	}
//-------------------------
		
	//Ejecutar la query para buscar los datos del usuario logeado.
	$query = "select * from usuarios where usuario like '$usuario'";
	if( !($salida = mysqli_query($enlace, $query)) )
	{
		die( "Error al consultar la tabla $bd.$tabla: <br><br>".mysql_error() );
	}
	  
	
	//Recojo en variables individuales la salida de la consulta.		
	while ($fila = mysqli_fetch_assoc($salida))
	{
	$nombre = $fila['nombre'];
	$apellidos = $fila['apellidos'];
	$direccion = $fila['direccion'];
	$dni = $fila['DNI'];
	$contrasenya = $fila['pass'];

	
	//Se las pinto al usuario en la pantalla.
	echo "<center>";
	echo "<h1>Bienvenido, <span style='font-size:2em'>$usuario</span></h1>";
	echo "<br>";
	echo "<span style='font-size:1.2em'>Tu nombre: <span style='font-style:italic'>$nombre </span></span><br>";
	echo "<span style='font-size:1.2em'>Tus apellidos: <span style='font-style:italic'>$apellidos </span></span><br>";
	echo "<span style='font-size:1.2em'>Tu direccion: <span style='font-style:italic'>$direccion </span></span><br>";	
	echo "<span style='font-size:1.2em'>Tu nombre de usuario: <span style='font-style:italic'>$usuario </span></span><br>";
	echo "<span style='font-size:1.2em'>Tu contrase&ntildea: <span style='font-style:italic'>$contrasenya </span></span><br>";
	}
	//echo "debug2<br>";
	

	//Al final de la pagina, un formulario para que el usuario pueda 
	//cambiar su contrasenya
?>
<br><br>
	<form action="http://192.168.200.112/consola.html" method="get">
		<input type="submit" style="width:225px;height:35" name="enviar" value="Acceder a Consola de Ataques">
	</form>	<center>


<?php
	if( isset( $_GET['enviarXSS'] ) || $_SESSION['cuadro'] == 1 )
	{
		echo "<center><div style='background-color:#FFC8C8; border: 1px solid black; width:650px' align='center'><p>En la caja del mensaje podemos inculuir un peque&ntildeo script (<a href='XSS_JS.php'>Ver algunos ejemplos de Javascript</a>) que ser&aacute ejecutado en el navegador de cualquiera que lo lea</p></center>";

		echo "<br>
	<form action='tablon.php' method='get' >
		<input type='submit' style='width:225px;height:35' name='verMensajes' value='Ver Mensajes'>
	</form>	<center>
<br><br>
		<br>
	<form action='confirmacionPost.php' method='get'>
		<textarea rows='10' cols='50' name='mensaje'>
Introduce aqu&iacute tu mensaje...
		</textarea><br>
		<input type='submit' name='enviarMensaje' value='Colgar Mensaje'>
	</form>";

	}
?>

	

</html>
